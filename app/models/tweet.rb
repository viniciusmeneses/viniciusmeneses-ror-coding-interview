# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 80 }
  validate :duplicated_tweet

  scope :affiliated, -> { joins(:user).where(user: { company_id: nil }) }

  private

  def duplicated_tweet
    return if body.blank? || user_id.blank?

    same_tweet = Tweet.where(body:, user_id:, created_at: 24.hours.ago..Time.current).exists?
    errors.add(:base, "You already created this tweet") if same_tweet
  end
end
