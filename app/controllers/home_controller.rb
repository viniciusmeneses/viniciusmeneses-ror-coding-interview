class HomeController < ApplicationController
  def index
    @tweets = Tweet.group(:user_id).includes(user: :company).order(created_at: :desc).limit(Api::TweetsController::TWEETS_PER_PAGE).to_a
  end
end
