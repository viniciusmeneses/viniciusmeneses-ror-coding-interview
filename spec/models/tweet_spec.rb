require 'rails_helper'

RSpec.describe Tweet, type: :model do
  let(:user) { create(:user) }

  context 'when body is greater than 80 characters' do
    let(:tweet) { Tweet.new(body: Faker::Alphanumeric.alpha(number: 88), user:) }

    it 'is invalid' do
      expect(tweet).to be_invalid
    end
  end

  context 'when body is less than 80 characters' do
    let(:tweet) { Tweet.new(body: Faker::Alphanumeric.alpha(number: 64), user:) }

    it 'is valid' do
      expect(tweet).to be_valid
    end
  end

  context 'when saving duplicated tweet within 24 hours' do
    let(:body) { 'Test' }

    let!(:old_tweet) { create(:tweet, body:, user:, created_at: 5.hours.ago)}
    let(:tweet) { Tweet.new(body:, user:) }

    it 'does not save' do
      expect(tweet.save).to be(false)
    end
  end
end
